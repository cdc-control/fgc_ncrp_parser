"""Parser for the FGC Network.Command Response Protocol (NCRP).

This module contains the parser for the FGC Network.Command Response Protocol (NCRP).

The parser is used to parse a string into a valid command object. The command object is then used to create a response object, which is then sent back to the client.

The parser is not meant to be used for validating commands. The command is validated in the parser, and the parser returns a command object. The command object is then used to create a response object, which is then sent back to the client.

"""

import re

from .fgc_ncrp import ncrp, errors

# define the parser function


def parse(data, logger):
  """The parse function gets a string and returns a valid Command object, or raises an exception.

  * The function checks if the data contains invalid characters ($, ;).
  * The function checks if the data matches the regex.
  * The function checks if the command is valid.
  * The function returns a Command object.

  """

  # check if invalid characters are present ($, ;)
  if data.find('$') != -1 or data.find(';') != -1:
    logger.error("Parser: $ or ; found in the data, invalid characters")
    raise errors.NcrpSyntaxError

  regex = gen_regex()

  # check if the data matches the regex

  # single matching
  match = re.search(regex, data)

  if match is None:
    logger.error("Parser: data does not match the regex")
    raise errors.NcrpSyntaxError

  try:
    tag = match.group(1)
    verb = match.group(2)
    device = match.group(3)
    ncrp_property = match.group(4)
    transaction_id = match.group(5)
    user_selector = match.group(6)
    array = match.group(7)
    payload = match.group(8)

    if verb.upper() in ['S', 'G']:
      return ncrp.Command(tag, verb, device, ncrp_property, transaction_id, user_selector, array, payload)
    else:
      logger.error("Parser: unknown command: " + verb)
      raise errors.UnknownCmd
  except Exception as exc:
    # print exception error
    logger.error(f"Parser: Unexpected exception occurred: {exc}")
    # we suggest the user to check the log to see what happened
    # this may be a misuse of the LogWaiting error, but it is the best we can do
    raise errors.LogWaiting


def gen_regex():
  """Since the NCRP is a regular language, we can use a regular expression to parse the command.

  The regex is generated from the following BNF:

  .. code-block:: bnf

    <command> ::= <preamble> <tag> <verb> <device> <property> <transaction_id> <user_selector> <array> <payload> <terminator>
    <preamble> ::= "!"
    <tag> ::= <string>
    <verb> ::= "s" | "S" | "g" | "G"
    <device> ::= <string> | <integer>
    <property> ::= <string> ("." <string>)*
    <transaction_id> ::= "<" <integer> ">"
    <user_selector> ::= "(" <integer> ")"
    <array> ::= "[" <array_id> | <array_fromid> | <array_fromid_toid> | <array_fromid_toid_step> "]"
    <array_id> ::= <integer>
    <array_fromid> ::= <integer> ","
    <array_fromid_toid> ::= <integer> "," <integer>
    <array_fromid_toid_step> ::= <integer> "," <integer> "," <integer>
    <payload> ::= <string>
    <terminator> ::= "\\n"


  Validation of the regex at https://regex101.com/r/aAyMZQ/1

  """

  # magic spell
  r_preamble = r'!'
  r_tag = r'([a-zA-Z0-9\_]{0,31})\s+'
  r_verb = r'([sgSG])\s+'
  r_device = r'(?:([a-zA-Z0-9]+)\:)?'
  r_property = r'([a-zA-Z0-9\_]*'
  r_additional_property = r'(?:\.[a-zA-Z0-9\_]+)*)'  # non capturing group
  r_transaction_id = r'(?:\<(\d*)\>)?'  # no range provided!
  r_user_selector = r'(?:\((\d\d?)\))?'  # should check range 0-31
  r_array_id = r'(?:\s*\d+\s*)?|'  # non capturing group
  r_array_fromid = r'(?:\s*\d+\s*,\s*)|'  # non capturing group
  r_array_fromid_toid = r'(?:\s*\d+\s*,\s*\d+\s*)|'  # non capturing group
  # non capturing group
  r_array_fromid_toid_step = r'(?:\s*\d+\s*,\s*\d+\s*,\s*\d+\s*)'
  r_payload = r'\s*([a-zA-Z0-9.,_\-\s]*)?'  # can either be values or options
  r_terminator = r'\n'

  r_array = r'(?:\[(' + r_array_id + r_array_fromid + \
      r_array_fromid_toid + r_array_fromid_toid_step + r')\])?'

  regex = r_preamble + r_tag + r_verb + r_device + r_property + r_additional_property + \
      r_transaction_id + r_user_selector + r_array + r_payload + r_terminator

  return regex
