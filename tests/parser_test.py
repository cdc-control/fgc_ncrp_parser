# test the parsing of the NCRP commands


import pytest
import mock

from .. import ncrp_parser

from ..fgc_ncrp.ncrp import errors


def test_NoTag():
  cmd = ncrp_parser.parse(
      "! s     foo.bar     aaa,bbb,ccc\n", logger=mock.Mock())
  assert cmd.tag == ''
  assert cmd.verb == 'S'
  assert cmd.device == 0
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.transaction_id == ''
  assert cmd.user_selector == ''
  assert cmd.payload == ['AAA', 'BBB', 'CCC']


def test_Tag():
  cmd = ncrp_parser.parse(
      "!t4g s     foo.bar     aaa,bbb,ccc\n", logger=mock.Mock())
  assert cmd.tag == 't4g'
  assert cmd.verb == 'S'
  assert cmd.device == 0
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.transaction_id == ''
  assert cmd.user_selector == ''
  assert cmd.payload == ['AAA', 'BBB', 'CCC']


def test_TagTooLong():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse(
        "!too_long_tag_45678901234567890123 s     foo.bar     aaa,bbb,ccc\n", logger=mock.Mock())


def test_NoCmd():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("zzz bbb\n", logger=mock.Mock())


def test_OnlyVerb():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s\n", logger=mock.Mock())


def test_InvalidChar():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s %\n", logger=mock.Mock())


def test_InvalidChar2():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s aaa%$\n", logger=mock.Mock())


def test_PropertyUser():
  cmd = ncrp_parser.parse("! s foo.bar(10)\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.user_selector == 10


def test_ManyPropertyUser():
  cmd = ncrp_parser.parse("! s foo.bar.blorb.nap(10)\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR.BLORB.NAP'
  assert cmd.user_selector == 10


def test_DevicePropertyUser():
  cmd = ncrp_parser.parse(
      "! s Device:foo.bar.blorb.nap(10)\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.device == 'Device'
  assert cmd.ncrp_property == 'FOO.BAR.BLORB.NAP'
  assert cmd.user_selector == 10


def test_ManyManyPropertyUser():
  cmd = ncrp_parser.parse(
      "! s foo.bar.a.b.c.d.e.f.g.blorb.nap(10)\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR.A.B.C.D.E.F.G.BLORB.NAP'
  assert cmd.user_selector == 10


def test_Nested():
  cmd = ncrp_parser.parse("! s foo.! s foo.bar(10)\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.user_selector == 10

# FGC3 implementation does not support text user names


def test_TextUser():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar(aaa)\n", logger=mock.Mock())


def test_TextUser2():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar(aaa,bbb)\n", logger=mock.Mock())


def test_Array():
  cmd = ncrp_parser.parse("! s foo.bar[1, 2, 3]\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1, 2, 3]


def test_ArrayTooLong():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar[1, 2, 3, 4]\n", logger=mock.Mock())


def test_TooManyArrays():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar[1, 2, 3][4, 5, 6]\n", logger=mock.Mock())


def test_ArrayNested():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar[1, 2, 3[4, 5, 6]]\n", logger=mock.Mock())

# wrong order


def test_ArrayUser():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar[1, 2, 3](10)\n", logger=mock.Mock())


def test_UserArray():
  cmd = ncrp_parser.parse("! s foo.bar(4)[1, 2, 3]\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1, 2, 3]
  assert cmd.user_selector == 4


def test_OnlyIndexFrom():
  cmd = ncrp_parser.parse("! s foo.bar(4)[1, ]\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1, '']
  assert cmd.user_selector == 4


def test_NoIndexTo():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar(4)[1, , 3]\n", logger=mock.Mock())


def test_OnlyIndexTo():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar(4)[, 2]\n", logger=mock.Mock())


def test_TransactionID():
  cmd = ncrp_parser.parse("! s foo.bar<12367>[1, 2]\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1, 2]
  assert cmd.transaction_id == 12367


def test_InvalidTransactionID():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! s foo.bar<1asdc1>[1, 2]\n", logger=mock.Mock())


def test_UserTransactionArray():
  cmd = ncrp_parser.parse("! s foo.bar<12367>[1, 2, 3]\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1, 2, 3]
  assert cmd.transaction_id == 12367


def test_DeviceUserTransactionArray():
  cmd = ncrp_parser.parse(
      "! s Device:foo.bar<12367>[1, 2, 3]\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.device == 'Device'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1, 2, 3]
  assert cmd.transaction_id == 12367


def test_Payload():
  cmd = ncrp_parser.parse(
      "! s foo.bar<12367>[1, 2, 3] aaab,,bbb,ccccdd\n", logger=mock.Mock())
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1, 2, 3]
  assert cmd.transaction_id == 12367
  assert cmd.payload == ['AAAB', '', 'BBB',  'CCCCDD']


def test_All():
  cmd = ncrp_parser.parse(
      "!tag s Device:foo.bar<345>(22)[2, 3]   abc, def, 2.4, 2aeddd, ffffFFF\n", logger=mock.Mock())
  assert cmd.tag == 'tag'
  assert cmd.verb == 'S'
  assert cmd.device == 'Device'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [2, 3]
  assert cmd.user_selector == 22
  assert cmd.transaction_id == 345
  assert cmd.payload == ['ABC', ' DEF', ' 2.4', ' 2AEDDD',  ' FFFFFFF']


def test_Get():
  cmd = ncrp_parser.parse(
      "!tag g Device:foo.bar<345>(22)[2, 3]\n", logger=mock.Mock())
  assert cmd.tag == 'tag'
  assert cmd.verb == 'G'
  assert cmd.device == 'Device'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [2, 3]
  assert cmd.user_selector == 22
  assert cmd.transaction_id == 345


def test_GetOptions():
  cmd = ncrp_parser.parse(
      "!tag g Device:foo.bar<345>(22)[2, 3] AAA,BC,23\n", logger=mock.Mock())
  assert cmd.tag == 'tag'
  assert cmd.verb == 'G'
  assert cmd.device == 'Device'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [2, 3]
  assert cmd.user_selector == 22
  assert cmd.transaction_id == 345
  assert cmd.payload == ['AAA', 'BC',  '23']


def test_GetSimple():
  cmd = ncrp_parser.parse("! g Bzz:dzz\n", logger=mock.Mock())
  assert cmd.verb == 'G'
  assert cmd.device == 'Bzz'
  assert cmd.ncrp_property == 'DZZ'


def test_GetDeviceID():
  cmd = ncrp_parser.parse("! g 10:foo\n", logger=mock.Mock())
  assert cmd.verb == 'G'
  assert cmd.device == 10


def test_Column():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! g :bar\n", logger=mock.Mock())


def test_MissingBracket():
  with pytest.raises(errors.NcrpSyntaxError):
    ncrp_parser.parse("! g foo.bar(4)[1, 2, 3\n", logger=mock.Mock())


if __name__ == '__main__':
  pytest.main([__file__])
