# FGC Network Command/Response parser

This file is part of the CDC Control project, but in principle it should be generic enough to allow to be used for anything.

This relies on the definitions supplied by https://gitlab.cern.ch/cdc-control/fgc_ncrp
